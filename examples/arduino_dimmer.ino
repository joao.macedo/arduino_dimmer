#include <ardumidi.h>
#include <AC_MultiDimmer.h>

#include <numeric>
#include <cmath>

/* 
Instructions/tools to setup QLC MIDI controler:
https://www.partsnotincluded.com/how-to-send-receive-midi-messages-over-serial/
https://www.qlcplus.org/download

On Windows:
* Create a loopback virtual midi port to setup as a QLC output device:
http://www.tobias-erichsen.de/software/loopmidi.html

* Connect the previously created virtual port (Midi In) to the arduino
  serial port (enable Serial<-> Midi Bridge):
http://projectgus.github.io/hairless-midiserial/#downloads

On Linux:
* Download and install ttymidi from:
https://github.com/robelix/hard-dj.git

* Create a virtual MIDI port from the Arduino USB, as follow:
ttymidi -s /dev/ttyUSB0 -b 115200 -v

*/


class DimFader {
    private:
    float threshold[2];
    float dimming;
    int fader_it, time_counter;

    public:
    int time_delta;
    int steps;

    DimFader(){

        threshold[0] = .09;
        threshold[1] = .4;
        time_delta = 40;
        steps = 256;
        time_counter = 0;
        fader_it = 0;
    }

    void triangle(AC_Dimmer& dimmer) {
        if (millis()%(2*time_delta) - time_counter > time_delta){
             time_counter = millis()%(2*time_delta);
             fader_it %= steps*2;
             if (fader_it > steps) {
                 dimming = 1. * (steps-fader_it%steps) / steps;
             } else if (fader_it <= steps) {
                 dimming = 1. * fader_it / steps;
             }

             dimming = threshold[0] + (threshold[1] - threshold[0]) * dimming;
             fader_it ++;
             dimmer.set_dimm_ratio(dimming);
        }
    }
};

//DimFader fader;



int pot_pin = 17;
int pot_value;

float get_dim_value_from_pot() {
    int dim_counts;
    int current_value = analogRead(pot_pin);
    if (pot_value != current_value) {
      pot_value = current_value*.1 + pot_value*.9;
      dim_counts = map(pot_value, 0, 1024, 0, 1000);
      //Serial.println(dim_counts/1000.);
    }
    return dim_counts/1000.;
}

AC_Dimmer dimmer1(1);
AC_Dimmer dimmer2(2);
AC_Dimmer dimmer3(3);
AC_Dimmer dimmer4(4);
AC_Dimmer dimmer5(5);
AC_Dimmer dimmer6(6);

/*
AC_Dimmer dimmer1(3);
AC_Dimmer dimmer2(4);
AC_Dimmer dimmer3(5);
AC_Dimmer dimmer4(8);
AC_Dimmer dimmer5(9);
AC_Dimmer dimmer6(14);
*/
/*

AC_Dimmer dimmer7(9);
AC_Dimmer dimmer8(10);
AC_Dimmer dimmer9(11);
AC_Dimmer dimmer10(12);
AC_Dimmer dimmer11(13);
AC_Dimmer dimmer12(14);
AC_Dimmer dimmer13(15);
AC_Dimmer dimmer14(16);
AC_Dimmer dimmer15(17);
AC_Dimmer dimmer16(18);
*/

void setup()
{

    //Start Serial COM for ardumidi
    Serial.begin(115200);
    
    // Set zero crossing interrupt
    // pin 2 -> interrupt 0; (Arduino Nano)

    //float thrashold[2] = {0.09, 0.97};
    //AC_Dimmer::begin(256, 0.008, thrashold, 2);
    //AC_Dimmer::begin(256, 0.008);
    AC_Dimmer::begin();

    // Set potentiometer pin mode
    pinMode(pot_pin, INPUT);


}

void loop()
{
    // Triangular wave form:
    //fader.triangle(dimmer1);
    //fader.triangle(dimmer3);


    float dimm_val;
    dimm_val = get_dim_value_from_pot();
    dimmer1.set_dimm_ratio(dimm_val);
/*
    dimmer2.set_dimm_ratio(dimm_val);
    dimmer3.set_dimm_ratio(dimm_val);
    dimmer4.set_dimm_ratio(dimm_val);
    dimmer5.set_dimm_ratio(dimm_val);
    dimmer6.set_dimm_ratio(dimm_val);
*/

    //AC_Dimmer::current_micros = micros();
    //if (AC_Dimmer::debug_ac_cycle_array.size() > 0) {

    //   float mean_val = 1.0 * std::accumulate(
    //       AC_Dimmer::debug_ac_cycle_array.begin(),
    //       AC_Dimmer::debug_ac_cycle_array.end(), 0
    //   ) / AC_Dimmer::debug_ac_cycle_array.size();

    //    Serial.print(AC_Dimmer::debug_ac_cycle_array.size());
    //    Serial.print(" ");
    //    Serial.print(std::accumulate(
    //       AC_Dimmer::debug_ac_cycle_array.begin(),
    //       AC_Dimmer::debug_ac_cycle_array.end(), 0
    //    ));
    //    Serial.print(" ");
    //    Serial.print(AC_Dimmer::debug_ac_cycle_array.front());
    //    Serial.print(" ");
    //    Serial.print(AC_Dimmer::debug_ac_cycle_array.back());
    //    Serial.print(" ");
    //    Serial.print(mean_val);
    //    Serial.print(" ");
    //    Serial.println(AC_Dimmer::debug_ac_cycle_mean);
    //}




    if (midi_message_available() > 0) {
        MidiMessage m = read_midi_message();
        //midi_command(lm.command, lm.channel, lm.param1, lm.param2);
        midi_controller_change(m.channel, m.param1, m.param2);

        // MIDI Channel has an offset. 0 -> 1
        if (m.command == MIDI_CONTROLLER_CHANGE and
            m.channel == 0) {

            //If you want to find out which controller was
            //changed, check the value of m.param1

            float dimm_value = 1.0 * m.param2 / 127.;
            // Used for the purpose of debuging the received MIDI messages:
            //midi_command(m.command, m.channel, m.param1, m.param2);
            midi_controller_change(m.channel, m.param1, m.param2);

            switch (m.param1) {

                //case 0: dimmer1.set_dimm_ratio(dimm_value);
                //    break;
                case 1: dimmer2.set_dimm_ratio(dimm_value);
                    break;
                case 2: dimmer3.set_dimm_ratio(dimm_value);
                    break;

                case 3: dimmer4.set_dimm_ratio(dimm_value);
                    break;
                case 4: dimmer5.set_dimm_ratio(dimm_value);
                    break;
                case 5: dimmer6.set_dimm_ratio(dimm_value);
                    break;
/*
        
                case 6: dimmer7.set_dimm_ratio(dimm_value);
                    break;        
                case 7: dimmer8.set_dimm_ratio(dimm_value);
                    break;        
                case 8: dimmer9.set_dimm_ratio(dimm_value);
                    break;
                case 9: dimmer10.set_dimm_ratio(dimm_value);
                    break;
                case 10: dimmer11.set_dimm_ratio(dimm_value);
                    break;          
                case 11: dimmer12.set_dimm_ratio(dimm_value);
                    break;          
                case 12: dimmer13.set_dimm_ratio(dimm_value);
                    break;          
                case 13: dimmer14.set_dimm_ratio(dimm_value);
                    break;          
                case 14: dimmer15.set_dimm_ratio(dimm_value);
                    break;
                case 15: dimmer16.set_dimm_ratio(dimm_value);
                    break;
*/

            }
        }
    }

}
