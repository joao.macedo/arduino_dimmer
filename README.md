# AC_MultiDimmer library

Despite born deprecated, due to the increasing usage of LED light bulbs instead of incandescent light bulbs. As also because there is already an offitial library (RobotDyn) which match the same requirements, this is more like a personal chalange...
This Library was inspired in the Arduino Controlled Light Dimmer project,
that implements a Trailing Edge Light dimmer for one single output AC
device.

https://www.instructables.com/Arduino-controlled-light-dimmer-The-circuit/


The AC_MultiDimmer library aims to provide a similar feature (Trailing Edge),
scaled up to multiple output AC devices with a totally independent drive.


## Circuit description:


The dimming process will be according with the elapsed time after each half
AC cycle start point and the TRIACs fireup, before the begining of a new half
cycle.
As much time it's waiten from the start point, until the TRIAC is fired, the more
close to the shutdown the output will be.
The Arduino receives an external interrupt input signal in the begining of each
AC cycle, through an AC zero crossing detection circuit (DB106 bridge rectifier
and a 4N25 optocoupler).
The Arduino output is made through a TRIAC driven optocoupler (MOC3021), which
fires the output devices respective TRIAC's Gate pin in the right time, taking
into account the zero crossing detection and the given dimming ratio float value
in a reange from 0.0 to 1.0. 



### Circuit components:


Bridge Rectifier (DB106)

     AC1  AC2
       |__| 
      |~  ~|
      |+__-|
       |  |
      DC+ DC-


Connections:

* AC1      to one of the 220V input terminals, with 33K Resistor,
           and to COMMON terminal for all the output AC dimmed
           devices load
* AC2      to the other 220V input terminal,
           also with 33K Resistor
* DC-      to Optocoupler LED Cathode
* DC+      to Optocoupler LED Anode
              (with optional 220 Ohm Resistor and LED)



Optocoupler (4N25)

                   ___
       LED Anode -|o  |- Base
     LED Cathode -|   |- Collector
            N.C. -|___|- Emitter


Connections:

* LED Anode    to Bridge Rectifier DC+
* LED Cathode  to Bridge Rectifier DC-
* Emitter      to Arduino Gnd
* Collector    to Arduino D2 (zero crossing),
               and to Arduino 5V with 10KOhm Resistor



Optocoupler (MOC3021)

                   ___
       LED Anode -|o  |- MT1
     LED Cathode -|   |- Substrate (N.C.)
            N.C. -|___|- MT2


Connections:

* LED Anode        to Arduino (or Shift Register) output pins,
                   with 220 Ohm Resistor
* LED Cathode      to Arduino Gnd
* Main Terminal 1  to AC terminal (220V) with 1kOhm Resistor,
                   in parallel with TRIAC MT2
* Main Terminal 2  to TRIAC Gate
   


TRIAC (BTB08)

        _____ 
       | BTB |
       | 08  |
       |_____|
        | | |
     MT1 MT2 Gate


Connections:

* Main Terminal 1  to the RESPECTIVE dimmed device load (220V)
* Main Terminal 2  to AC terminal (220V),
                   in parallel with MOC3021 MT1
* Gate             to MOC3021 MT2
   


Optional feature using Shift Register, is enabled by using the followig
define directive: "#define SHIFT_REG", before the library include.
The Shift Register's implementation depends on the SPI Library, thus
the respective source code shall be made available in the Arduino
libraries path.

Reference:
https://lastminuteengineers.com/74hc595-shift-register-arduino-tutorial/
https://forum.arduino.cc/t/does-it-matter-which-digital-pins-i-hook-up-to-a-shift-register/673785/2

Shift Register (74HC595)

           ___
      QB -| U |- VIn
      QC -|   |- QA
      QD -|   |- Serial In Data
      QE -|   |- OE
      QF -|   |- RCLK
      QG -|   |- SRCLK
      QH -|   |- SRCLR
     Gnd -|___|- QH'
       

Connections:

* VIn             to Arduino 5V
* Serial In Data  to Arduino D11
* OE              to Arduino Gnd
* RCLK            to Arduino D10
* SRCLK           to Arduino D13
* SRCLR           to Arduino 5V
* Gnd             to Arduino Gnd




## Code details:


The AC_MultiDimmer uses Arduino Timer 2 Interrupt Service Routine (ISR) to
manage each device respective TRIAC fireing up order, and the external
intrrupt (default 0) to detect the zero crossing point of the AC cycle.
All the output devices are stored in a "vector" that is sorted by its dimming
value, in the begining of each new AC half cycle.


As an alternative to "std::vector", that wasn't available at the time this code was
done, this library implements the "varray" class to provide the required features. 
Almost everything used in the code regarding to C++ base clases and functions,
was done by Michael Brodsky in its PG library (ISO-compliant C++ Standard
Library for the Arduino). Thanks to its great job, this work became much more easy.

IMPORTANT: This library depends on the PG library, thus both libraries source code shall
be made available at the Arduino libraries path.

PG (Pretty Good) library source code:
https://github.com/Michael-Brodsky/pg
