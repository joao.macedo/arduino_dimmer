
#ifndef ACMD_MAXSIZE
  #define ACMD_MAXSIZE 16
#endif

#ifndef NCYCLES
#define NCYCLES 100
#endif

#define SHIFT_REG


#ifdef SHIFT_REG
  #include <SPI.h>
  #ifndef PIN_SPI_SS
  #define PIN_SPI_SS 10
  #endif
#endif

#include <Arduino.h>
#include <pg.h>

#include <iterator>
#include <array>
#include <algorithm>
 
const int MAXSIZE = ACMD_MAXSIZE;



template<class T, std::size_t N, template<class = T, std::size_t = N> typename Alloc = std::array>
class varray: public std::array<T, N> {

    private:
        std::size_t next_pos;
        std::array<T, 0> empty_array;

    public:
        varray<T, N, Alloc>() : std::array<T, N>(), next_pos(0) {};

        varray<T, N, Alloc>(std::initializer_list<T> list) : std::array<T, N>() {
            this->assign(list);
        };

        void assign(std::initializer_list<T> list) {
            //next_pos = N > list.size() ? list.size() : N;
            //std::copy(list.begin(), list.begin() + next_pos, this->begin());
            this->assign(list.begin(), list.end());
        };

        void assign(std::size_t count, T elem) {
             std::array<T, N>::fill(elem);
             next_pos = N > count ? count : N;
        };

        void assign(typename Alloc<T, N>::const_iterator it_begin,
                    typename Alloc<T, N>::const_iterator it_end) {
            std::size_t src_size = it_end - it_begin;
            next_pos = N > src_size ? src_size : N;
            std::copy(it_begin, it_begin + next_pos, this->begin());
        };

        void push_back(T elem) {
            if (next_pos < N) {
                next_pos += 1;
                (*this)[next_pos - 1] = elem;
            }
        };

        T pop_back() {
            if (next_pos > 0) {
                T result = (*this)[next_pos - 1];
                next_pos -= 1;
                return result;
            } else {
                return empty_array.back();
            }
        };

        T pop_front() {
            if (next_pos > 0) {
                T result = (*this)[0];
                std::move(this->begin() + 1, this->end(), this->begin());
                next_pos -= 1;
                return result;
            } else {
                return empty_array.back();
            }
        };

        typename Alloc<T, N>::iterator end() {
            return this->begin() + next_pos;
        };

        typename Alloc<T, N>::iterator rbegin() {
            return end();
        };

        // c++11
        typename Alloc<T, N>::iterator cend() {
            return end();
        };

        // c++11
        typename Alloc<T, N>::iterator crbegin() {
            return end();
        };

        T& back() {
            return (*this)[empty() ? empty_array.back() : next_pos - 1];
        };

        T& operator[](std::size_t pos) {
            return next_pos > pos ? std::array<T, N>::operator[](pos) : empty_array.back();
        };

        const T& operator[](std::size_t pos) const {
            return next_pos > pos ? std::array<T, N>::operator[](pos) : empty_array.back();
        };

        bool empty() {
            return (next_pos == 0);
        };

        void clear() {
            next_pos = 0;
        };

        void resize(std::size_t pos) {
            next_pos = pos < N ? pos : N;
        };

        std::size_t size() {
            return next_pos;
        };

        std::size_t max_size() {
            return N;
        };
};




class AC_Dimmer {

    //private:

    static varray<AC_Dimmer*, MAXSIZE> dimmers;
    static varray<std::pair<unsigned short, unsigned char>, MAXSIZE> flush_queue;
    static varray<std::pair<unsigned short, unsigned char>, MAXSIZE>::iterator flush_it; 
    static varray <unsigned short, NCYCLES> final_counts;
    static unsigned short ac_cycle_counts, shutdown_counts,
                          total_counts, timer_max_counts,
                          error_counts, timer_remain_counts;
    static float ac_cycle_micros;
    static float ratio_threshold[2];
    static int n_full_power;
    static long int spi_max_speed;
    static int prescalar_registers;
    unsigned short dimm_counts;
    bool shutdown, full_power;
    unsigned char ac_load_pin;
#ifdef SHIFT_REG 
    static int shift_reg_value;
#endif

    public:

    AC_Dimmer(unsigned char load_pin);
    void set_dimm_ratio(float dimm_ratio);
    // prescalar can be any of 1024, 256, 128, 64, 32, 8, 1
    static void begin(int prescalar=32,
                      float error=0.00,
                      float threshold[2]=NULL,
                      int zero_cross_int=2
#ifdef SHIFT_REG
                      // F_CPU: 16000000
                    , long int spi_freq=F_CPU
#endif
                      );
    static void (*isr_callback)();
    static void start_calibration();
    static void update_calibration();
    static void sort_queuing_list();
    static void start_flush();
    static void update_flush();
    static void end_flush();
};
