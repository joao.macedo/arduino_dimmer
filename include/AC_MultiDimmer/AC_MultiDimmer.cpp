#include "AC_MultiDimmer.h"


AC_Dimmer::AC_Dimmer(unsigned char load_pin) {
#ifdef SHIFT_REG
    ac_load_pin = ceil(pow(2, load_pin - 1));
#else
    ac_load_pin = load_pin;
#endif
    shutdown = true;
    dimmers.push_back(this);
};


void AC_Dimmer::begin(
    int prescalar,
    float error,
    float threshold[2],
    int zero_cross_pin 
#ifdef SHIFT_REG
    ,long int spi_freq
#endif
) {

/*
       Note: Only timer2 implementation is done
 
       About Arduino timers:
       timer0 and timer2 are 8bit (256 steps)
       while timer1 is 16bit (65535 steps)

       Prescalar possible values for timer0 and timer1:
       1, 8, 64, 256, 1024
    
       for timer2:
       1, 8, 32, 64, 128, 256, 1024
    
       TCCR2B registers for prescalar:
    
       CS22  CS21  CS20   DESCRIPTION
         0     0     0    Timer/Counter0 Disabled 
         0     0     1    No Prescaling (Clock / 1)
         0     1     0    Clock / 8
         0     1     1    Clock / 32
         1     0     0    Clock / 64
         1     0     1    Clock / 128
         1     1     0    Clock / 256
         1     1     1    Clock / 1024
       CS bits
    
       Computing each time step duration, in microseconds for Arduino (16MHz):
           time_step = prescalar / 16MHz
           
       The AC cycle frequency in Europe is about 50Hz.
       As the zero crossing event happens twice in a cycle (ascending and
       descending curves), each AC half cycle, where the dimming process
       takes place, takes about 10000 microseconds (100Hz).

       Computing total number of counts for the AC half cycle
           ac_cycle_counts = 10000 x 16 / prescalar
    
       Example with prescalar value of 1024:
           time_step = 1024 / 16MHz = 64.0 microseconds
           ac_cycle_counts = 10000 / 64.0 = 156 steps

*/
    
    timer_max_counts = 255; // timer2

    if (threshold != NULL) {
        ratio_threshold[0] = threshold[0];
        ratio_threshold[1] = threshold[1];
    }

#ifdef SHIFT_REG 
    spi_max_speed = spi_freq;
    shift_reg_value = 0x00;
    pinMode(PIN_SPI_SS, OUTPUT);
    SPI.begin();
#else
    // Set output AC load pins
    std::array<AC_Dimmer*, MAXSIZE>::iterator it; 
    for (it = dimmers.begin(); it != dimmers.end(); ++it) {
        pinMode((*it)->ac_load_pin, OUTPUT);
    }
#endif

    // Set the Timer/Counter Control Registers Prescalar
    switch (prescalar) {
        case 1: prescalar_registers = (1 << CS20);
            break;
        case 8: prescalar_registers = (1 << CS21);
            break;
        case 32: prescalar_registers = (1 << CS21) | (1 << CS20);
            break;
        case 64: prescalar_registers = (1 << CS22);
            break;
        case 128: prescalar_registers = (1 << CS22) | (1 << CS20);
            break;
        case 256: prescalar_registers = (1 << CS22) | (1 << CS21);
            break;
        case 1024: prescalar_registers = (1 << CS22) | (1 << CS21) | (1 << CS20);
    }

    // Cleanup Timer/Counter Control Registers (TCCRnA/B)
    TCCR2A = 0x00;
    TCCR2B = 0x00;

    // Set prescalar:
    TCCR2B |= prescalar_registers; 

    // Disable Timer Output Compare Interrupts A and B (OCIEnA/B):
    TIMSK2 &= ~(1 << OCIE2A); 
    TIMSK2 &= ~(1 << OCIE2B); 

    // Disable Timer Overflow Interrupt (TOIEn):
    TIMSK2 &= ~(1 << TOIE2);

    // Compute ac_cycle_counts calibration after delay...
    isr_callback = update_calibration;

    // Set zero crossing interrupt to start_calibration callback
    // pin 2 -> interrupt 0; (Arduino Nano)
    attachInterrupt(digitalPinToInterrupt(zero_cross_pin),
                    AC_Dimmer::start_calibration, RISING);
    delay(1500);

    // Get AC half cycle counts median value
    // from NCYCLES size sample
    std::sort(final_counts.begin(), final_counts.end());
    ac_cycle_counts = final_counts[int(floor(NCYCLES/2.))];
    error_counts = ac_cycle_counts * error;
    //Serial.println(ac_cycle_counts);
    //Serial.println(error_counts);
    shutdown_counts = ac_cycle_counts *
                      (1. -  ratio_threshold[0]);
    isr_callback = update_flush;

    // Set zero crossing interrupt
    // pin 2 -> interrupt 0; (Arduino Nano)
    attachInterrupt(digitalPinToInterrupt(zero_cross_pin),
                    AC_Dimmer::start_flush, RISING);
};


void AC_Dimmer::set_dimm_ratio(float dimm_ratio){
/*
    dimm_ratio range: [0.0, 1.0]
    1.0 -> full power
    0.0 -> power off
*/
    shutdown = false;
    full_power = false;

    // Update dimm_counts:
    dimm_counts = ac_cycle_counts * (1. - dimm_ratio);
    if (dimm_ratio < ratio_threshold[0]) {
        shutdown = true;
    } else if (dimm_ratio > ratio_threshold[1]) {
        full_power = true;
    }

};


void AC_Dimmer::start_calibration() {

    // Compute final counts
    unsigned short latest_cycle_counts;
    latest_cycle_counts = total_counts + TCNT2;

    if (final_counts.size() == NCYCLES){
        final_counts.pop_front();
        final_counts.push_back(latest_cycle_counts);
    } else {
        final_counts.push_back(latest_cycle_counts);
    }

    // Reset total counts and timer counter
    total_counts = 0;
    TCNT2 = 0;

    // Reset prescalar registers
    TCCR2B |= prescalar_registers; 

    // Set Output Compare Register value
    OCR2A = timer_max_counts;

    // Enable Output Compare Match Mode A
    TIMSK2 |= (1 << OCIE2A);

}


void AC_Dimmer::update_calibration() {
    // Update total_counts value with the latest
    // timer cycle counts (timer_max_counts)
    total_counts += timer_max_counts;    

    // Reset timer counter
    TCNT2 = 0;

}


void AC_Dimmer::sort_queuing_list() {

    // Clear flush_queue
    flush_queue.clear();

    // Update all dimmers respective pair (dimm_counts, ac_load_pin)
    // into the new cycle flush_queue
    n_full_power = 0;
    for (varray<AC_Dimmer*, MAXSIZE>::iterator dimm_it=dimmers.begin();
        dimm_it != dimmers.end(); ++dimm_it) {
        // Don't add shutdown dimmers to the next flush_queue
        if (!(*dimm_it)->shutdown) {
            // Update number of full power dimmers
            if((*dimm_it)->full_power) {
                ++n_full_power;
            }
            flush_queue.push_back(std::pair<unsigned short, unsigned char>(
                    (*dimm_it)->dimm_counts, (*dimm_it)->ac_load_pin
            ));
        }
    }

    // Sort flush_queue, based on each dimmer dimm_counts
    std::sort(flush_queue.begin(), flush_queue.end());
};


void AC_Dimmer::start_flush() {

    // Force all pins to shutdown while starting a new AC cycle
    end_flush();
    sort_queuing_list();

    // First guess for the remain counts
    // to interrupt the next timer cycle
    timer_remain_counts = timer_max_counts; 

    if (flush_queue.empty()) {
        flush_it = flush_queue.end();
    } else {
        // Initialize flush_it iterator and
        // flush full_power pins
#ifdef SHIFT_REG
        shift_reg_value = 0x00;
#endif
        for (flush_it = flush_queue.begin();
             flush_it != flush_queue.begin() + n_full_power;
             ++flush_it) {

#ifdef SHIFT_REG
            shift_reg_value |= flush_it->second;
#else
            digitalWrite(flush_it->second, HIGH);
#endif
        }

#ifdef SHIFT_REG
        SPI.beginTransaction(
            SPISettings(spi_max_speed , MSBFIRST, SPI_MODE0)
        );
        digitalWrite(PIN_SPI_SS, LOW);
        SPI.transfer(shift_reg_value);
        digitalWrite(PIN_SPI_SS, HIGH);
        SPI.endTransaction();
#endif

        // Reset Timer Counter value (TCNTn)
        if (flush_it->first < timer_max_counts){
            // Update timer_remain_counts value for
            // the next timer cycle interrupt
            timer_remain_counts = flush_it->first;
        }
    }

    // Reset prescalar registers
    TCCR2B |= prescalar_registers; 

    // Set Output Compare Register value
    OCR2A = timer_remain_counts;

    // Reset total counts and timer counter
    total_counts = 0;
    TCNT2 = 0;

    // Enable Output Compare Match Mode A
    TIMSK2 |= (1 << OCIE2A);

};


void AC_Dimmer::update_flush() {

    // Update total_counts value with the latest
    // timer cycle counts (timer_remain_counts)
    total_counts += timer_remain_counts;

    // First guess for the remain counts
    // to interrupt the next timer cycle
    timer_remain_counts = timer_max_counts;

    if (total_counts >= shutdown_counts and
        flush_it->second == flush_queue.end()->second) {
        // Shutdown all dimmers after total_counts
        // reach shutdown_counts
        end_flush();
    } else {
        if (flush_it != flush_queue.end()) {
            if (flush_it->first <= total_counts) {
                // Fire up load pins 
                unsigned short current_dim_counts = total_counts
                                                    + error_counts;
                while (flush_it != flush_queue.end()) {
                    if (flush_it->first <= current_dim_counts) {

#ifdef SHIFT_REG
                        shift_reg_value |= flush_it->second;
#else
                        digitalWrite(flush_it->second, HIGH);
#endif
                        flush_it++;
                    } else {
                         break;
                    }
#ifdef SHIFT_REG
                SPI.beginTransaction(
                    SPISettings(spi_max_speed , MSBFIRST, SPI_MODE0)
                );
                digitalWrite(PIN_SPI_SS, LOW);
                SPI.transfer(shift_reg_value);
                digitalWrite(PIN_SPI_SS, HIGH);
                SPI.endTransaction();
#endif
                }
            }

            // Update timer_remain_counts value for
            // the next timer cycle interrupt
            if (flush_it != flush_queue.end() and
                flush_it->first <
                total_counts + timer_max_counts) {
                    timer_remain_counts = flush_it->first -
                                          total_counts;
            }
        }
    }

    // Set Output Compare Register value
    OCR2A = timer_remain_counts;

    // Reset timer counter
    TCNT2 = 0;

};


void AC_Dimmer::end_flush() {

    // Clear prescalar registers
    TCCR2B = 0x00;

    // Disable Timer interupt
    TIMSK2 &= ~(1 << TOIE2);

    // Shutdown all triacs from previous flush_queue:
#ifdef SHIFT_REG
    shift_reg_value = 0x00;
    SPI.beginTransaction(
        SPISettings(spi_max_speed , MSBFIRST, SPI_MODE0)
    );
    digitalWrite (PIN_SPI_SS, LOW);
    SPI.transfer(shift_reg_value);
    digitalWrite (PIN_SPI_SS, HIGH);
    SPI.endTransaction();
#else
    varray<AC_Dimmer*, MAXSIZE>::iterator it;
    for (it = dimmers.begin();
         it != dimmers.end(); ++it) {
        digitalWrite((*it)->ac_load_pin, LOW);
    }
#endif
};

void (*AC_Dimmer::isr_callback)();

varray<AC_Dimmer*, MAXSIZE> AC_Dimmer::dimmers;
varray<std::pair<unsigned short,
                 unsigned char>, MAXSIZE> AC_Dimmer::flush_queue;
varray<std::pair<unsigned short,
                 unsigned char>, MAXSIZE>::iterator AC_Dimmer::flush_it;
varray <unsigned short, NCYCLES> AC_Dimmer::final_counts;
unsigned short AC_Dimmer::ac_cycle_counts,
               AC_Dimmer::shutdown_counts,
               AC_Dimmer::timer_max_counts,
               AC_Dimmer::timer_remain_counts,
               AC_Dimmer::error_counts,
               AC_Dimmer::total_counts;
float AC_Dimmer::ac_cycle_micros = 10000; // Europe
float AC_Dimmer::ratio_threshold[2] = {0.09, 0.97}; // [0.0, 1.0]
int AC_Dimmer::prescalar_registers;
int AC_Dimmer::n_full_power;
#ifdef SHIFT_REG 
int AC_Dimmer::shift_reg_value;
long int AC_Dimmer::spi_max_speed;
#endif
  

ISR(TIMER2_COMPA_vect){
    AC_Dimmer::isr_callback();
}
